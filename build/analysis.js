const { execSync } = require('child_process');
const path = require('path');
const ConsoleUtils = require('../ConsoleUtils');

const buildAnalysis = () => {
    let error;
    try {
        const analysisPath = path.join(__dirname, '../..', 'gces-analysis');
        const paketPath = path.join(analysisPath, '.paket');
    
        const analysisPluginPath = path.join(analysisPath, 'src/Gces.AnalysisModel.Plugin');
        const analysisPluginAppPath = path.join(analysisPluginPath, 'ClientApp');
    
        ConsoleUtils.info(`[1/3] cd ${paketPath} and run dotnet paket restore`);
        execSync(`dotnet paket restore`, { cwd: paketPath, stdio: 'inherit' });
    
        ConsoleUtils.info(`[2/3] cd ${analysisPluginPath} and run dotnet restore`);
        execSync('dotnet restore Gces.AnalysisModel.Plugin.csproj', { cwd: analysisPluginPath, stdio: 'inherit' });
    
        ConsoleUtils.info(`[3/3] cd ${analysisPluginAppPath} and run yarn && yarn run build`);
        execSync('yarn && yarn run build', { cwd: analysisPluginAppPath, stdio: 'inherit' });
    } catch(e) {
        error = e;
    }
    return new Promise((resolve, reject) => {
        error ? reject(`analysis: ${error}`) : resolve('analysis');
    })
}

module.exports = buildAnalysis;