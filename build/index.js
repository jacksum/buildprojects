const buildAnalysis = require('./analysis');
const buildCommon = require('./common');
const buildDashboard = require('./dashboard');
const buildServer = require('./server');
const ConsoleUtils = require('../ConsoleUtils');

let analysisPromise = Promise.resolve();
let commonPromise = Promise.resolve();
let dashboardPromise = Promise.resolve();
let serverPromise = Promise.resolve();

analysisPromise = buildAnalysis();
commonPromise = buildCommon();
dashboardPromise = buildDashboard();
serverPromise = buildServer();

Promise.allSettled([analysisPromise, commonPromise, dashboardPromise, serverPromise])
.then((projects) => {
    projects.forEach(project => {
        if (project.value && project.status === 'fulfilled') {
            ConsoleUtils.success(`Build ${project.value} successfully!`);
        } else if (project.status === 'rejected'){
            ConsoleUtils.error(`Build failed -> ${project.reason}`);
        }
    });
});
