const { execSync } = require('child_process');
const path = require('path');
const ConsoleUtils = require('../ConsoleUtils');

const buildServer = () => {
    let error;
    try {
        const serverPath = path.join(__dirname, '../..', 'gces-server');
        const paketPath = path.join(serverPath, '.paket');
        const serverPluginPath = path.join(serverPath, 'src/Gces.Server');
        
        const schedulerPluginAppPath = path.join(serverPath, 'src/Gces.Scheduler.Plugin/clientApp');
        const shareResourcesPluginAppPath = path.join(serverPath, 'src/Gces.SharedResources.Plugin/clientApp');
    
        ConsoleUtils.info(`[1/4] cd ${paketPath} and run dotnet paket restore`);
        execSync(`dotnet paket restore`, { cwd: paketPath, stdio: 'inherit' });
    
        ConsoleUtils.info(`[2-1/4] cd ${serverPluginPath} and run dotnet restore`);
        execSync('dotnet restore', { cwd: serverPluginPath, stdio: 'inherit' });
        ConsoleUtils.info(`[2-2/4] cd ${serverPluginPath} and run yarn && yarn run build`);
        execSync('yarn && yarn run build', { cwd: serverPluginPath, stdio: 'inherit' });
    
        ConsoleUtils.info(`[3/4] cd ${schedulerPluginAppPath} and run yarn && yarn run build`);
        execSync('yarn && yarn run build', { cwd: schedulerPluginAppPath, stdio: 'inherit' });
    
        ConsoleUtils.info(`[4/4] cd ${shareResourcesPluginAppPath} and run yarn && yarn run build`);
        execSync('yarn && yarn run build', { cwd: shareResourcesPluginAppPath, stdio: 'inherit' });
    } catch (e) {
        error = e;
    }
    return new Promise((resolve, reject) => {
        error ? reject(`server: ${error}`) : resolve('server');
    });
}

module.exports = buildServer;