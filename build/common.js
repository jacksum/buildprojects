const { execSync } = require('child_process');
const path = require('path');
const ConsoleUtils = require('../ConsoleUtils');

const buildCommon = () => {
    let error;
    try {
        const commonPath = path.join(__dirname, '../..', 'gces-common');
        const paketPath = path.join(commonPath, '.paket');
    
        const datasetPath = path.join(commonPath, 'src/Gces.Dataset.Plugin');
        const datasetAppPath = path.join(datasetPath, 'ClientApp');
        const accountAppPath = path.join(commonPath, 'src/Gces.Account.Plugin', 'ClientApp');
    
        ConsoleUtils.info(`[1/4] cd ${paketPath} and run dotnet paket restore`);
        execSync(`dotnet paket restore`, { cwd: paketPath, stdio: 'inherit' });
    
        ConsoleUtils.info(`[2/4] cd ${datasetPath} and run dotnet restore`);
        execSync('dotnet restore', { cwd: datasetPath, stdio: 'inherit' });
    
        ConsoleUtils.info(`[3/4] cd ${datasetAppPath} and run yarn && yarn run build`);
        execSync('yarn && yarn run build', { cwd: datasetAppPath, stdio: 'inherit' });
        
        ConsoleUtils.info(`[4/4] cd ${accountAppPath} and run yarn && yarn run build`);
        execSync('yarn && yarn run build', { cwd: accountAppPath, stdio: 'inherit' });
    } catch(e) {
        error = e;
    }
    return new Promise((resolve, reject) => {
        error ? reject(`common: ${error}`) : resolve('common');
    });
}

module.exports = buildCommon;