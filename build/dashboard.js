const { execSync } = require('child_process');
const path = require('path');
const ConsoleUtils = require('../ConsoleUtils');

const buildDashboard = () => {
    let error;
    try {
        const dashboardPath = path.join(__dirname, '../..', 'gces-dashboard');
        const paketPath = path.join(dashboardPath, 'backend/.paket');
    
        const dashboardPluginPath = path.join(dashboardPath, 'backend/DashboardPlugin');
        const dashboardPlugintAppPath = path.join(dashboardPath, 'frontend');
    
        ConsoleUtils.info(`[1/3] cd ${paketPath} and run dotnet paket restore`);
        execSync(`dotnet paket restore`, { cwd: paketPath, stdio: 'inherit' });
    
        ConsoleUtils.info(`[2/3] cd ${dashboardPluginPath} and run dotnet restore`);
        execSync('dotnet restore', { cwd: dashboardPluginPath, stdio: 'inherit' });
    
        ConsoleUtils.info(`[3/3] cd ${dashboardPlugintAppPath} and run npm i && npm run build`);
        execSync('npm i && npm run build', { cwd: dashboardPlugintAppPath, stdio: 'inherit' });
    } catch(e) {
        error = e;
    }
    return new Promise((resolve, reject) => {
        error ? reject(`dashboard: ${error}`) : resolve('dashboard');
    })
}

module.exports = buildDashboard;