const { spawn } = require('child_process');
const { promisify } = require('util');
const spawnPromise = promisify(spawn);
const path = require('path');
const ConsoleUtils = require('./ConsoleUtils');

const monetHostPath = path.join(__dirname, '..', 'monetdb-cluster-management/src/Gces.MonetDb.Cluster.Bootstrap.Host');
const monetServerPath = path.join(__dirname, '..', 'monetdb-cluster-management/src/Gces.MonetDb.Cluster.Server');
const cotWorkerPath = path.join(__dirname, '..', 'gces-common/src/Gces.CotWorker');

ConsoleUtils.info(`[1/3] cd ${monetHostPath} and run dotnet run`);
const host = spawnPromise('dotnet', ['run'], { cwd: monetHostPath, detached: true });

ConsoleUtils.info(`[2/3] cd ${monetServerPath} and run dotnet run`);
const server = spawnPromise('dotnet', ['run'], { cwd: monetServerPath, detached: true });

ConsoleUtils.info(`[3/3] cd ${cotWorkerPath} and run dotnet run`);
const cot = spawnPromise('dotnet', ['run'], { cwd: cotWorkerPath, detached: true });

Promise.all([
    host,
    server,
    cot
]).then(() => {
    ConsoleUtils.success('COT worker is running!');
}).catch((e) => {
    ConsoleUtils.error(e);
});