const simpleGit = require('simple-git');
const _ = require('lodash');

const path = require('path');
const fs = require('fs');

const ws = fs.createWriteStream('gces-dashboard.csv');
ws.once('open', () => {
  console.log('流打开了');
});
ws.once('close', () => {
  console.log('流关闭了');
});
ws.write('Hash,Name,Email,Date,Insertions,Deletions,Changed,Subject\n');

const dashboard = async () => {
  const dashboardPath = path.join(__dirname, '..', 'gces-dashboard');
  const git = simpleGit(dashboardPath);
  const { all } = await git.log({
    '--stat': 4096,
    '--fixed-strings': true,
    '--no-merges': true,
    ':(exclude)*assets/*': true,
    ':(exclude)*asserts/*': true,
    ':(exclude)*lib/*': true,
    ':(exclude)*static/*': true,
    ':(exclude)*build/*': true,
    ':(exclude)*gc\.spread*': true,
    ':(exclude)*package-lock\.json': true,
    format: {
      hash: '%h',
      name: '%an',
      email: '%ae',
      date: '%aI',
      subject: '%s',
    },
  });
  all.forEach(({ hash, name, email, date, diff, subject }) => {
    ws.write(`${hash},${name},${email},${date},${diff?.insertions ?? 0},${diff?.deletions ?? 0},${diff?.changed ?? 0},${subject}\n`);
  });
  ws.end();
};

dashboard();