const colors = require('colors');

module.exports = class ConsoleUtils {
    static log(str) {
        console.log(str);
    }
    static info(str) {
        console.log(colors.yellow(str));
    }
    static error(str) {
        console.log(colors.red(str));
    }
    static success(str) {
        console.log(colors.green(str));
    }
}