const path = require('path');
const { execSync } = require('child_process');
const ConsoleUtils = require('../ConsoleUtils');

exports.Branches = [
    'develop',
    'release/v8.1',
];

exports.Projects = [
    'gces-dashboard',
    'gces-server',
    'gces-common',
    'gces-analysis',
    'monetdb-cluster-management',
];

const getPath = (project) => path.join(__dirname, '..', '..', project);

exports.checkout = (project, branch) => {
    const targetPath = getPath(project);
    ConsoleUtils.info(`checkout ${project}`);
    execSync(`git checkout ${branch}`, { cwd: targetPath, stdio: 'inherit' });
};

exports.fetch = (project) => {
    const targetPath = getPath(project);
    ConsoleUtils.info(`fetch ${project}`);
    execSync('git fetch', { cwd: targetPath, stdio: 'ignore' });
};

exports.pull = (project, branch) => {
    const targetPath = getPath(project);
    ConsoleUtils.info(`pull ${project}`);
    execSync(`git pull origin ${branch}`, { cwd: targetPath, stdio: 'inherit' });
};

exports.stash = (project) => {
    const targetPath = getPath(project);
    ConsoleUtils.info(`stash ${project}`);
    execSync(`git stash`, { cwd: targetPath, stdio: 'inherit' });
};

exports.stashPop = (project) => {
    const targetPath = getPath(project);
    ConsoleUtils.info(`stash pop ${project}`);
    execSync(`git stash pop`, { cwd: targetPath, stdio: 'inherit' });
};
