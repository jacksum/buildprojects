const ConsoleUtils = require('../ConsoleUtils');
const { Projects, checkout, stash, stashPop, Branches } = require('./utils');

const branch = process.argv[2];
if (!Branches.includes(branch)) {
  ConsoleUtils.error(`${branch} is not a valid branch.`);
  process.exit(1);
}

ConsoleUtils.info(`git checkout ${branch}`);
Projects.forEach((p) => {
  try {
    checkout(p, branch);
  } catch {
    try {
      stash(p);
      checkout(p, branch);
      stashPop(p);
    } catch (ex) {
      ConsoleUtils.error(`checkout ${p} failure. ${ex}`);
    }
  }
});
