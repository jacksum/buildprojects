const ConsoleUtils = require('../ConsoleUtils');
const { Branches, Projects, pull, stash, stashPop } = require('./utils');

const origin = process.argv[2];
if (!Branches.includes(origin)) {
  ConsoleUtils.error(`${origin} is not a valid branch.`);
  process.exit(1);
}

ConsoleUtils.info(`git pull from ${origin}`);
Projects.forEach((p) => {
  try {
    pull(p, origin);
  } catch {
    try {
      stash(p);
      pull(p, origin);
      stashPop(p);
    } catch (ex) {
      ConsoleUtils.error(`pull ${p} failure. ${ex}`);
    }
  }
});
