# README #

### 1. Purpose & Function

* **Build projects**: when we need to debug backend codes, we should build projects before start server, this repository helps up to build projects conveniently(`npm run build`). Including:
    * **gces-server**, **gces-common**, **gces-analysis**, **gces-dashboard**.
    * backend and frontend.

* **Git batch command**: before we build projects, we should make sure all projects are on same branches or based on same branches(to avoid to different backend dependencies causing failure run). Here provides some commands for convenient:
    * `npm run co develop`： run `git checkout develop` for all projects, you can filter any projects out by modifying code in `git/utils.js`. Also, you can checkout other branches.
    * `npm run pull develop`: run `git pull origin develop` for all projects.
    * **Warning**: there may appear some errors when checkout and pull, we will try to fix them but may fail(e.g, conflict code). If not fixed yet, please solve them seperately.

* **Start cot worker**: we need to use cot worker when request dataset:
    * `npm run cot`: start cot worker(require repositories **gces-common**, **monetdb-cluster-management**).

### 2. How to start server?

#### 2.1 Prerequisite
* Install **Visual Studio**(with .NET 6), **Node 16**, **yarn**, **PostgreSQL 13**, **MonetDB SQL**.
* Clone **gces-server**, **gces-common**, **gces-analysis**, **gces-dashboard**, **monetdb-cluster-management**, **buildProjects** from bitbucket to one local folder, for example, named **wyn**.
* Ensure all projects use same branches except **buildProjects**. Branch name is decided by your feature/bug, if a feature will be finished in **release/v6.1**, then make sure all branches are **release/v6.1** or based on **release/v6.1**.

#### 2.2 Build Projects
* `cd buildProjects` and run `npm i && npm run build`, wait a moment.
    * **Warning**: the step may have some errors, please solve them before run server.

#### 2.3 Start server
1. Open `Gces.Server.sln` in **gces-server** with Visual Studio.
2. Add plugins(refer to **3. How to add plugin in server?**):
    * DashboardPlugin(by Project)
    * AccountManagementPlugin(by Dll)
    * DatasetPlugin(by Dll)
    * AnalysisModelPlugin(by Dll)
3. Try to run project and access `http://localhost:42002`.

### 3. How to add plugin in server?
1. Project reference, which used to debug code, take **DashboardPlugin** as an example:
    * Right-click on **Dependencies** and choose **Add Reference** in Gces.Server, and checked **DashboardPlugin**.
    * Open **ServicesExtensions.cs**, add a line `new PluginInfo(new DashboardsPlugin(Configuration))` to `AddPlugins` method. Press `Alt + Enter` to add reference.
2. Dll reference, use the method if no need to debug plugin, take **DatasetPlugin** as an example:
    * Download **dataset-and-account\*.zip** from **gces-common** Downloads tab.
    * Unzip the file.
    * Add the path of `Gces.Dataset.Plugin.dll` and `Gces.Account.Plugin.dll` under `Plugins -> Paths` in `appsettings.Development.json`.